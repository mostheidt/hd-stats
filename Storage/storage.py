import yaml
from sqlalchemy import create_engine, or_
from sqlalchemy.orm import sessionmaker
from flask import Flask, jsonify, request
import pymysql
from base import Base
from team import Team
from game import Game
import csv

app_conf_file = "./app_conf.yml"
# This breaks PEP8 because there will be a separate file for testing. It won't be a constant in that case.
# log_conf.yml will go here when implemented.

with open(app_conf_file, 'r') as f:
    app_config = yaml.safe_load(f.read())
    db_conf = app_config['datastore']

DB_ENGINE = create_engine(f"mysql+pymysql://{db_conf['user']}:{db_conf['password']}@"
                          f"{db_conf['hostname']}:{db_conf['port']}/{db_conf['db']}")
                          # If the DB has issues w/ volume, increase pool_size and max_overflow

Base.metadata.bind = DB_ENGINE

DB_SESSION = sessionmaker(bind=DB_ENGINE)


# So, with the way this test is going to work, games are going to reference teams by id as normal. Which is fine.
# The problem is, teams have to be added in exactly this order or games don't refer to the right team.
# This won't be an issue in practice since the UI will abstract it all.
# Might require that midseason joiners have their stats for said season compiled through both their ID and the old team's ID.
def add_teams_from_csv():
    with open('test_data/teams.csv', 'r') as f:
        for i in csv.DictReader(f):
            session = DB_SESSION()
            team = Team(i['name'], i['location'], i['username'])
            session.add(team)
            session.commit()
            session.close()


def add_games_from_csv():
    with open('test_data/final_scores.csv', 'r') as f:
        for i in csv.DictReader(f):
            if i['OT'] == 'ot':
                i['OT'] = True
            else:
                i['OT'] = False
            if i['SO'] == 'Final SO':
                i['SO'] = True
            else:
                i['SO'] = False
            session = DB_SESSION()
            game = Game(i['Date'], i['Team A'], i['Team B'], i['Home Goals'], \
                i['Away Goals'], i['OT'], i['SO'])
            session.add(game)
            session.commit()
            session.close()


def get_team_by_id(team_id):
    session = DB_SESSION()
    team = session.get(Team, team_id)
    team_dict = team.to_dict()
    team_dict['points'] = calculate_points(team_id)
    return team_dict


def calculate_points(team_id):
    session = DB_SESSION()
    games = session.query(Game).filter(or_(Game.home_team_id == team_id, \
        Game.away_team_id == team_id))
    game_list = []
    for game in games:
        game_list.append(game.to_dict())
    session.close()
    points = 0
    for game in game_list:
        if game['home_team_id'] == team_id:
            # Should check somewhere else (maybe inserting results) that home goals != away goals
            if game['home_goals'] > game['away_goals']:
                points += 2
            elif game['ot'] or game['so']:
                points += 1
        elif game['away_team_id'] == team_id:
            if game['away_goals'] > game['home_goals']:
                points += 2
            elif game['ot'] or game['so']:
                points +=1
    return points


def calculate_standings():
    # In the future this will take a league parameter and be filtered to that.
    # Look into ways to filter this by conference and division as well.
    team_list = []
    for i in range(1,31):
        points = calculate_points(i)
        # A method to get team info by id would be nice. Probably needed for frontend.
        team_dict = get_team_by_id(i)
        team_list.append({'team_id':i, 'points':points, \
            'name':f"{team_dict['location']} {team_dict['name']}"})
    return sorted(team_list, reverse=True, key=lambda k: k['points'])


app = Flask(__name__)

@app.route('/standings')
def get_standings():
    return jsonify(calculate_standings())

@app.route('/team/info/<team_id>')
# Is this crap naming? Absolutely.
# If it wasn't used by other functions (eg standings) I'd just define the logic here.
def get_team_by_id_route(team_id):
    return jsonify(get_team_by_id(int(team_id)))

@app.route('/team/points/<team_id>')
def get_team_points_by_id(team_id):
    points = calculate_points(int(team_id))
    return jsonify(points)

if __name__ == '__main__':
    app.run(debug=True, port=8080, use_debugger=False, use_reloader=False)
