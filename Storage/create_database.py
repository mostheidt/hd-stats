"""This code sets up the DB in MySQL.
Not exactly happy with using raw SQL here. could become a hassle when expanding tables.
Might be a way to do this through SQLAlchemy, if so that's probably better."""

import mysql.connector
import yaml

with open('Storage/app_conf.yml', 'r') as f:
    app_config = yaml.safe_load(f.read())
    db_conf = app_config['datastore']

conn = mysql.connector.connect(host=db_conf['hostname'], user=db_conf['user'],\
    password=db_conf['password'], database=db_conf['db'], port=db_conf['port'])

c = conn.cursor()

# If someone can sign off on this SQL that'd be great.
# If there's issues, even with naming conventions etc, feel free to make a merge request with fixes.
# Or just point them out, I can likely fix it once I know what to look for.

c.execute('''
CREATE TABLE teams
(id INT NOT NULL AUTO_INCREMENT,
name VARCHAR(20) NOT NULL,
location VARCHAR(20) NOT NULL,
username VARCHAR(20),
CONSTRAINT PK_team PRIMARY KEY (id))
''')

c.execute('''
CREATE TABLE games
(id INT NOT NULL AUTO_INCREMENT,
date_played VARCHAR(100) NOT NULL,
home_team_id INT NOT NULL,
away_team_id INT NOT NULL,
home_goals INTEGER NOT NULL,
away_goals INTEGER NOT NULL,
ot BOOLEAN NOT NULL,
so BOOLEAN NOT NULL,
CONSTRAINT PK_game PRIMARY KEY (id),
CONSTRAINT FK_home_team
FOREIGN KEY (home_team_id)
    REFERENCES teams(id),
CONSTRAINT FK_away_team
FOREIGN KEY (away_team_id)
    REFERENCES teams(id))
''')

conn.commit()
conn.close()
