import mysql.connector
import yaml

with open('Storage/app_conf.yml', 'r') as f:
    app_config = yaml.safe_load(f.read())
    db_conf = app_config['datastore']

conn = mysql.connector.connect(host=db_conf['hostname'], user=db_conf['user'],\
    password=db_conf['password'], database=db_conf['db'], port=db_conf['port'])

c = conn.cursor()

# NOTE - dropping teams first results in an FK error because games refers to teams.
# As such, test that this script works after any FK changes or additions.
c.execute('''
DROP TABLE games, teams
''')

conn.commit()
conn.close()
