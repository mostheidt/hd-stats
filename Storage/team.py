from sqlalchemy import Column, Integer, String
from base import Base


class Team(Base):

    __tablename__ = "teams"

    id = Column(Integer, primary_key=True)
    name = Column(String(20), nullable=False)
    location = Column(String(20), nullable=False)
    # arena = Column(String(30))
    username = Column(String(20))
    # region = Column(String(20))
    # league = Column(String(10), nullable=False) # Can validation be done better with another type?
    # points info will be moved to another table. May use another solution down the line.
    """     games_played = Column(Integer, nullable=False)
    # How to make sure record matches up with GP?
    # I'd love to just make it a string but separate ints make more sense.
    wins = Column(Integer, nullable=False)
    losses = Column(Integer, nullable=False)
    ot_losses = Column(Integer, nullable=False)
    so_losses = Column(Integer, nullable=False) """

    def __init__(self, name, location, username):
        """Initializes a new team"""
        self.name = name
        self.location = location
        self.username = username

    def to_dict(self):
        dict = {}
        dict['id'] = self.id
        dict['name'] = self.name
        dict['location'] = self.location
        dict['username'] = self.username
        return dict
