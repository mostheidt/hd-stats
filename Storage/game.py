from sqlalchemy import Column, Integer, String, DateTime, ForeignKey, Boolean
from sqlalchemy.orm import relationship
from base import Base
from team import Team
import datetime


class Game(Base):

    __tablename__ = "games"

    id = Column(Integer, primary_key=True)
    date_played = Column(DateTime)
    # Not confident in the implementation of these FKs. Revisit later.
    # If it's incorrect, will be quite obvious.
    home_team_id = Column(Integer, ForeignKey(Team.id))
    away_team_id = Column(Integer, ForeignKey(Team.id))
    home_goals = Column(Integer)
    away_goals = Column(Integer)
    # OT and SO are mutually exclusive - game can't end in an OT period and a shootout.
    # Find a way to enforce this through table structure?
    ot = Column(Boolean)
    so = Column(Boolean)

    def __init__(self, date_played, home_team, away_team, home_goals, away_goals, \
        ot=False, so=False):
        self.date_played = date_played
        self.home_team_id = home_team
        self.away_team_id = away_team
        self.home_goals = home_goals
        self.away_goals = away_goals
        # PyLint doesn't like variables under 3 characters. There's a way to get around this later.
        self.ot = ot
        self.so = so

    def to_dict(self):
        dict = {}
        dict['id'] = self.id
        dict['date_played'] = self.date_played
        dict['home_team_id'] = self.home_team_id
        dict['away_team_id'] = self.away_team_id
        dict['home_goals'] = self.home_goals
        dict['away_goals'] = self.away_goals
        dict['ot'] = self.ot
        dict['so'] = self.so
        return dict
